
#!/usr/bin/python
# -*- coding: utf-8 -*-
'''\nScript - Generate Json Partidos Final FPC 
Author: andari
Usage: python script.py"
''' 
import sys
import datetime
import timeit
import json
from xml.etree import cElementTree
import xmlestadisticas
import os.path as path

def finalesfpc ():
	archivopy = "finalesfpc"
	jsonconf = json.loads(open('conf-finalesfpc.json').read())
	jsontorneos = json.loads(open('torneos.json').read())
	formato = "%Y%m%d"
	hoy = datetime.datetime.now()
	diaactivo = hoy.strftime(formato)
	arraypartidos = {}
	for i, equipos in enumerate(jsonconf["equipos"]):
			torneoequipo =  jsonconf["equipos"][equipos]["torneo"]
			equipo = jsonconf["equipos"][equipos]["equipo"]
			diapartido = jsonconf["equipos"][equipos]["dia"]
			estadoequipo = jsonconf["equipos"][equipos]["activo"] #retorna 1: activo 0:desactivo
			if estadoequipo == 1:
				for i, torneo in enumerate(jsontorneos):
					if torneoequipo == torneo:
					   idtorneo = jsontorneos[torneo]['id']
					   season = jsontorneos[torneo]['temporada']
					   cadena = "opta/srml-{0}-{1}-results.xml"
					   cadena = cadena.format(idtorneo,season)
					   if path.exists(cadena):
					   	  tree = cElementTree.parse(cadena)
					   	  haypartidoshoy = xmlestadisticas.mostrarinformaciondepartidos(archivopy,torneoequipo,idtorneo,season, tree, diapartido, None, None, equipo)
					   	  if len(haypartidoshoy) >= 1:
					   	  	if not arraypartidos.has_key(torneo):
					   	  		arraypartidos[torneo] = []
					   	  	arraypartidos[torneo].append(haypartidoshoy)
				with open('json/finalesfpc'+'.json', 'w') as file:
					json.dump(arraypartidos, file)




	# 	if path.exists(cadena):	
	# 	   tree = cElementTree.parse(cadena)
	# 	   haypartidoshoy = xmlestadisticas.mostrarinformaciondepartidos(torneo,idtorneo,season, tree, diaactivo, cantidaddias, None, equipos)
	#  	   if len(haypartidoshoy) >= 1:
	#  	   	  if not arraypartidos.has_key(torneo):
	# 				arraypartidos[torneo] = []
	# 		  arraypartidos[torneo].append(haypartidoshoy)
	# with open('json/marcadoresenlinea'+'.json', 'w') as file:
 #          json.dump(arraypartidos, file)



class jsonfinalesfpc():
      def __init__(self):
         self.finalesfpc = finalesfpc()
 

if __name__ == "__main__":
   #jsonpartidosdeldia()
   print(timeit.timeit(finalesfpc,number=1))



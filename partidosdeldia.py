#!/usr/bin/python
# -*- coding: utf-8 -*-
'''\nScript - Generate Json Partidos del Dia
Author: andari
Usage: python script.py"
''' 
import sys
import datetime
import timeit
import json
from xml.etree import cElementTree
import xmlestadisticas
import os.path as path

def partidosdeldia ():
	archivopy = "partidosdeldia"
	jsonconf = json.loads(open('conf-partidosdeldia.json').read())
	formato = "%Y%m%d"
	hoy = datetime.datetime.now()
	diaactivo = hoy.strftime(formato)
	cantidaddias = jsonconf["cantidad_dias"]
	arraypartidos = {}
	for i, torneo in enumerate(jsonconf["torneos"]):
		idtorneo = jsonconf["torneos"][torneo]['id']
		season = jsonconf["torneos"][torneo]['season']  
		equipos = jsonconf["torneos"][torneo]['equipos']  
		cadena = "opta/srml-{0}-{1}-results.xml"
		cadena = cadena.format(idtorneo,season)
		if path.exists(cadena):	
		   tree = cElementTree.parse(cadena)
		   haypartidoshoy = xmlestadisticas.mostrarinformaciondepartidos(torneo,idtorneo,season, tree, diaactivo, cantidaddias, None, equipos)
	 	   if len(haypartidoshoy) >= 1:
	 	   	  if not arraypartidos.has_key(torneo):
					arraypartidos[torneo] = []
			  arraypartidos[torneo].append(haypartidoshoy)
	with open('json/marcadoresenlinea'+'.json', 'w') as file:
          json.dump(arraypartidos, file)



class jsonpartidosdeldia():
      def __init__(self):
         self.partidosdeldia = partidosdeldia()
 

if __name__ == "__main__":
   #jsonpartidosdeldia()
   print(timeit.timeit(partidosdeldia,number=1))



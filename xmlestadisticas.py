#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
import getopt
from xml.etree import cElementTree
import datetime
from datetime import timedelta
from datetime import date
import operator
import json
import os.path as path
import partidosdeldia

def ordenarpordia(archivoxml, tipo):
    arraycalendario = {}
    formato = "%Y%m%d"
    for elem in archivoxml.iterfind('SoccerDocument/MatchData'):  #datos de cada partido
        data = {}
        matchinfo = {}
        stat = {}
        goles = {}
        infoteam = {}
        infogolL = {}
        infogolV = {}
        teamdata = {}
        contador = 0
        cgoles = 0
        data['detail_id'] = elem.get('detail_id')
        data['last_modified'] = elem.get('last_modified')
        data['timestamp_accuracy_id'] = elem.get('timestamp_accuracy_id')
        data['timing_id'] = elem.get('timing_id')
        data['uID'] = elem.get('uID')
        fecha = next(elem.iterfind('MatchInfo/Date'))
        fechapartido = fecha.text
        fechastr = datetime.datetime.strptime(fecha.text, '%Y-%m-%d %H:%M:%S')
        fechacolombia = horaverano(fechastr)
        fechastrc = datetime.datetime.strptime(fechacolombia, '%Y-%m-%d %H:%M:%S')
        #print("colombia =>",fechacolombia,"fecha Feed =>",fechaPartido)
        dia = fechastrc.strftime("%Y%m%d")
        for info in elem.iterfind('MatchInfo'):
            matchinfo ['MatchDay'] = info.get('MatchDay')
            matchinfo['MatchType'] = info.get('MatchType')
            matchinfo['MatchWinner'] = info.get('MatchWinner')
            matchinfo['Period'] = info.get('Period')
            matchinfo['RoundType'] = info.get('RoundType')
            matchinfo['RoundNumber'] = info.get('RoundNumber')
            if info.get('GroupName') is not None:
                matchinfo['GroupName'] = info.get('GroupName')
            else:
            	matchinfo['GroupName'] = None
            matchinfo['Venue_id'] = info.get('Venue_id')
            matchinfo['Date'] = fechapartido
            matchinfo['FechaPartido'] = fechacolombia
        data['MatchInfo'] = matchinfo
        for infostat in elem.findall('Stat'):
            stat[infostat.get('Type')] = infostat.text
            if not stat.has_key('City'):
                   stat['City'] = ''
        data['Stat'] = stat
        for infoteamdata in elem.iterfind('TeamData'):
            infoteam['HalfScore'] = infoteamdata.get('HalfScore')
            infoteam['Score'] = infoteamdata.get('Score')
            infoteam['ShootOutScore'] = infoteamdata.get('ShootOutScore')
            infoteam['PenaltyScore'] = infoteamdata.get('PenaltyScore')
            infoteam['Side'] = infoteamdata.get('Side')
            infoteam['TeamRef'] = infoteamdata.get('TeamRef')
            if infoteam['Side'] == 'Home':
                teamdata['equipo1'] = infoteam
                infoteam = {"":""}
            else:
                teamdata['equipo2'] = infoteam

        data['TeamData'] = teamdata
        if not arraycalendario.has_key(dia):
            arraycalendario[dia] = []

        arraycalendario[dia].append(data)

    resultado = sorted(arraycalendario.items(), key=operator.itemgetter(0))
    if tipo == None:
       return arraycalendario
    else:
    	return resultado


def mostrarinformaciondepartidos(torneo, idtorneo, season, xml, diaactual, cantidaddias, partidosfinalizados, equipo):
	formato = "%Y%m%d"
	partidoscalendario = ordenarpordia(xml,1)
	diaactivo = datetime.datetime.now() 
	fdiaactivo  = diaactivo.strftime(formato)
	partidosdeldia = []
	idspartidosdeldia = []
	partidosenjuego = {}
	partidosjson = {}
	cantidadmaximadias = 8
	
	for x in range(1,cantidadmaximadias): #Se optiene los partidos según la cantidad de días a mostrar
		for key,values in partidoscalendario:
			if key == fdiaactivo:
				partidosdeldia.extend(values)
		fdiaactivo = datetime.datetime.now() + timedelta(days=x)
		fdiaactivo = fdiaactivo.strftime(formato)
   
	if partidosdeldia is not None: #Si hay partidos en el día leemos el archivo xml del partido
		partidoEquipoSeleccionado = None
	 	for datos in partidosdeldia:
	 		idpartidodia = datos['uID']
	 		fechapartido = datos['MatchInfo']['Date']
	 		diapartido = datos['MatchInfo']['FechaPartido']
	 		periodopartido = datos['MatchInfo']['Period']
	 		idspartidosdeldia.append(idpartidodia)
	if idspartidosdeldia is not None:
		for idpartido in idspartidosdeldia:
			idreemplazar = idpartido[1:]
			cadenamatch = "opta/srml-{0}-{1}-f{2}-matchresults.xml"
			cadenamatch = cadenamatch.format(idtorneo,season,idreemplazar)
			if path.exists(cadenamatch):
			   tree = cElementTree.parse(cadenamatch)
			   if not partidosenjuego.has_key(idreemplazar):
			   	      partidosenjuego[idreemplazar] = []
			   partidosenjuego[idreemplazar] = tree
	partidos = partidosdeldia   
	partidosenjuego = partidosenjuego
	datosjson = crearjson(partidos, partidosenjuego, partidosfinalizados, xml, torneo, idtorneo, season, equipo)
	return datosjson

def crearjson(partidos, partidosenjuego, partidosfinalizados, xml, torneo, idtorneo,season , equipo):
	arraypartidosdia = {}
	array = []
	for partido in partidos:
		partidos = {}
		enlacepartido = {}
		data = {}
		periododelpartido = partido['MatchInfo']['Period']
		if periododelpartido == "Postponed":
		   continue
		if partidosfinalizados is not None and periododelpartido != "FullTime":
		   continue
		idpartido = partido['uID'][1:]
		fecha = partido['MatchInfo']['FechaPartido']
		fechastr = datetime.datetime.strptime(fecha,'%Y-%m-%d %H:%M:%S')
		diapartido = fechastr.strftime("%Y%m%d")
		fechapartido = partido['MatchInfo']['FechaPartido']
		teamreflocal = partido['TeamData']['equipo1']['TeamRef']
		teamrefvisitante = partido['TeamData']['equipo2']['TeamRef']
		nombrelocal = teamlist(teamreflocal, xml, 1)
		nombrevisitante = teamlist(teamrefvisitante, xml, 1)
		fichaequipolocal = teamlist(teamreflocal, xml, 2)
		fichaequipovisitante = teamlist(teamrefvisitante, xml, 2)
		if type(equipo) is list:
			if fichaequipolocal in equipo or fichaequipovisitante in equipo:
				pass
			else:
				continue
		equipolocal = teamlist(fichaequipolocal, xml, 3)
		equipovisitante = teamlist(fichaequipovisitante, xml, 3)
		abreviaturalocal = equipolocal['abreviatura']
		abreviaturavisitante = equipovisitante['abreviatura']
		if fichaequipolocal is None or fichaequipovisitante is None:
			pass
		idequipolocal = teamreflocal[1:]
		idequipovisitante = teamrefvisitante[1:]
		enlacepartido['equipoA'] =  fichaequipolocal
		enlacepartido['equipoB'] =  fichaequipovisitante
		enlacepartido['id'] = idpartido
		estadio = partido['Stat']['Venue']
		ciudad = partido['Stat']['City']
		ganador = '0'
		goleslocal = ''
		golespenallocal = ''
		golesvisitante = ''
		golespenalvisitante = ''
		mostrarpenales = None
		detalle = detallepartido(partidosenjuego, idpartido)
		periodoenjuego = detalle.get('Periodo')
		if periodoenjuego is not None:
			for clave, valor in detalle.iteritems(): 
				periododelpartido = detalle['Periodo']
				mostrarmarcadores = detalle['TeamData']['equipo1']['Score']
				if periododelpartido != "PreMatch" and  mostrarmarcadores is not None:
					goleslocal = detalle['TeamData']['equipo1']['Score']
					golesvisitante = detalle['TeamData']['equipo2']['Score']
					if goleslocal > golesvisitante:
						ganador = detalle['TeamData']['equipo1']['TeamRef']
					elif goleslocal < golesvisitante:
						ganador = detalle['TeamData']['equipo2']['TeamRef']
						mostrarpenales = detalle['TeamData']['equipo1']['ShootOutScore']
						if mostrarpenales is not None:
							golespenallocal = detalle['TeamData']['equipo1']['ShootOutScore']
							golespenalvisitante = detalle['TeamData']['equipo2']['ShootOutScore']
							if golespenallocal > golespenalvisitante:
								ganador = detalle['TeamData']['equipo1']['TeamRef']
							elif golespenallocal < golespenalvisitante:
								ganador = detalle['TeamData']['equipo2']['TeamRef']

		else:
			mostrarmarcadores = partido['TeamData']['equipo1']['Score'] 
			if partido['MatchInfo']['Period'] != "PreMatch" and mostrarmarcadores is not None:
				goleslocal = partido['TeamData']['equipo1']['Score']
				golesvisitante = partido['TeamData']['equipo2']['Score']
				if goleslocal > golesvisitante:
					ganador = partido['TeamData']['equipo1']['TeamRef']
				elif goleslocal < golesvisitante:
					ganador = partido['TeamData']['equipo2']['TeamRef']
					mostrarpenales = partido['TeamData']['equipo1']['ShootOutScore']
					if mostrarpenales is not None:
						golespenallocal = partido['TeamData']['equipo1']['ShootOutScore']
						golespenalvisitante = partido['TeamData']['equipo2']['ShootOutScore']
						if golespenallocal > golespenalvisitante:
							ganador = partido['TeamData']['equipo1']['TeamRef']
						elif golespenallocal < golespenalvisitante:
							ganador = partido['TeamData']['equipo2']['TeamRef']

		while switch(periododelpartido):
			if case('PreMatch'):
				estadopartido = 'por-jugar'
				fecha = partido['MatchInfo']['Date']
				fechastr = datetime.datetime.strptime(fecha, '%Y-%m-%d %H:%M:%S')
        		fechacolombia = horaverano(fechastr)
        		mensajepartido = fechacolombia #mostrar fecha partido si es el dia actual mostrar hora
        		break
			if case('FullTime'):
				estadopartido = 'finalizado'
				mensajepartido = 'FT'
				break
			if case('HalfTime'):
				estadopartido = 'encurso'
				mensajepartido = 'MT'
				break
			if case('FirstHalf'):
				estadopartido = 'encurso'
				tiempomsj = detalle.get('Tiempo') + "'"
				mensajepartido = tiempomsj
				break
			if case('SecondHalf'):
				estadopartido = 'encurso'
				tiempomsj = detalle.get('Tiempo') + "'"
				mensajepartido = tiempomsj
				break
			if case('Live'):
				estadopartido = 'encurso'
				mensajepartido = 'En juego'
				break
			estadopartido = 'encurso'
			if detalle is not None:
				tiempomsj = detalle.get('Tiempo') + "'"
				mensajepartido = tiempomsj
			else:
				mensajepartido = 'En juego'
			break
		rbaseimagenesopta = 'http://images.akamai.opta.net/football/team/badges_20/{0}.png'
		rbasexmlcomentariosopta = 'http://www.futbolred.com/contenido/opta/commentary-{0}-{1}-{2}-es.xml'
		basexmlcomentariosopta = rbasexmlcomentariosopta.format(idtorneo,season,idpartido)
		if estadopartido == 'por-jugar':
			xmlcomentarios = None
		else:
			xmlcomentarios = basexmlcomentariosopta
		jsonpartidosdeldia = {'fecha':diapartido,
							  'id':idpartido,
							  'estadio':estadio,
							  'ciudad':ciudad,
							  'estadodelpartido':estadopartido,
							  'mensajepartido':mensajepartido,
							  'mostrarmarcador':mostrarmarcadores,
							  'mostrarpenaltis':mostrarpenales,
							  'ganador':ganador,
							  'equipolocal':{'id':idequipolocal,
							                 'abreviatura':abreviaturalocal,
							                 'nombre':nombrelocal,
							                 'ficha':fichaequipolocal,
							                 'goles':goleslocal,
							                 'golespenal':golespenallocal,
							                 'imagen':rbaseimagenesopta.format(idequipolocal)},
							   'equipovisitante':{'id':idequipovisitante,
							                 'abreviatura':abreviaturavisitante,
							                 'nombre':nombrevisitante,
							                 'ficha':fichaequipovisitante,
							                 'goles':golesvisitante,
							                 'golespenal':golespenalvisitante,
							                 'imagen':rbaseimagenesopta.format(idequipovisitante)},
							   'url':enlacepartido,
							   'torneo':torneo,
							   'xmlcomentarios':xmlcomentarios
							  }
		#array.append(jsonpartidosdeldia)	
		if not arraypartidosdia.has_key(diapartido):
			arraypartidosdia[diapartido] = []
		arraypartidosdia[diapartido].append(jsonpartidosdeldia)
	return arraypartidosdia

def detallepartido (partidosenjuego, idpartido):
	data = {}
	infoteam = {}
	teamdata = {}
	for i, xmlpartido in enumerate(partidosenjuego):
		if xmlpartido == idpartido:
			xmlenjuego = partidosenjuego[xmlpartido]
			for elem in xmlenjuego.iterfind('SoccerDocument/MatchData/MatchInfo'):
				periodo = elem.get('Period')
			data["Periodo"] = periodo
			for infoteamdata in xmlenjuego.iterfind('SoccerDocument/MatchData/TeamData'):
				infoteam['HalfScore'] = infoteamdata.get('HalfScore')
				infoteam['Score'] = infoteamdata.get('Score')
				infoteam['PenaltyScore'] = infoteamdata.get('PenaltyScore')
				infoteam['Side'] = infoteamdata.get('Side')
				infoteam['TeamRef'] = infoteamdata.get('TeamRef')
				if infoteam['Side'] == 'Home':
					teamdata['equipo1'] = infoteam
					infoteam = {}
				else:
					teamdata['equipo2'] = infoteam
			data["TeamData"] = teamdata
			for stat in xmlenjuego.iterfind('SoccerDocument/MatchData/Stat'):
				if stat.get('Type') == "match_time":
				   tiempo =  stat.text
			data['Tiempo'] = tiempo
	if data is not None:
		return data

def array_merge( first_array , second_array ):
	if isinstance( first_array , list ) and isinstance( second_array , list ):
		return first_array + second_array
	elif isinstance( first_array , dict ) and isinstance( second_array , dict ):
		return dict( list( first_array.items() ) + list( second_array.items() ) )
	elif isinstance( first_array , set ) and isinstance( second_array , set ):
		return first_array.union( second_array )
	return False

class switch(object):
    value = None
    def __new__(class_, value):
        class_.value = value
        return True

def case(*args):
    return any((arg == switch.value for arg in args))

def teamlist(idbuscar, tree, tipo):
	if tipo == 1:
		equipos = {}
		for elem in tree.iterfind('SoccerDocument/Team'):  #datos de cada equipo
		    for nombreEquipos in elem.iterfind('Name'):
		    	data = {}
		    	data['idEquipo'] = elem.get('uID')
		    	data['Nombre'] = nombreEquipos.text
		    	if not equipos.has_key('Equipos'):
		    		equipos['Equipos'] = []
		    	equipos['Equipos'].append(data)
		    if idbuscar == data['idEquipo']:
		    	nombre = data['Nombre']
		    	break
		    else:
		    	nombre = ''
	elif tipo == 2:
		jsonequipos = json.loads(open('equipos.json').read())
		for clases in jsonequipos["clases"]:
		    if idbuscar == clases:
		       nombre = jsonequipos["clases"][clases]
		       break
		    else:
		         nombre = ''   
	else:
		jsonequipos = json.loads(open('equipos.json').read())
		for equipo in jsonequipos["equipos"]:
			if idbuscar == equipo:
				nombre = jsonequipos["equipos"][equipo]
				break
			else:
				nombre = ''	

	return nombre

def horaverano (fechaxml):
	calendarioverano = {'2016':{'03':27,'10':30},
						 '2017':{'03':26,'10':29},
						 '2018':{'03':25,'10':28},
						 '2019':{'03':31,'10':27},
						 '2020':{'03':29,'10':25},
						 '2021':{'03':28,'10':31},
						 '2022':{'03':27,'10':30}
						  } 

	formato = "%Y%m%d"
	formatolargo = "%Y-%m-%d %H:%M:%S"
	formatoanio = "%Y"
	formatomes = "%m"
	dia = datetime.datetime.now() 
	diaactual = dia.strftime(formato)
	anioactual = dia.strftime(formatoanio)
	mesactual = dia.strftime(formatomes)
	mesinicioverano = '03'
	mesfinverano = '10'
	fechaactualini = str(anioactual) + str(mesactual) + str(diaactual)
	fechainicioverano = str(anioactual) + str(mesinicioverano) + str(calendarioverano[anioactual][mesinicioverano])
	fechafinverano = str(anioactual) + str(mesfinverano) + str(calendarioverano[anioactual][mesfinverano])

	if fechaactualini >= fechainicioverano and fechaactualini < fechafinverano:
		horaverano = timedelta(hours=6)
	else:
		horaverano = timedelta(hours=5)
	fechaxml = fechaxml - horaverano
	fechacol = fechaxml.strftime(formatolargo)
	return fechacol

def datospartido(periododelpartido, partido):
	datospartido = []
	while switch(periododelpartido):
			if case('PreMatch'):
				estadopartido = 'por-jugar'
				datospartido.append(estadopartido)
				fecha = partido['MatchInfo']['Date']
				fechastr = datetime.datetime.strptime(fecha, '%Y-%m-%d %H:%M:%S')
        		fechacolombia = horaverano(fechastr)
        		mensajepartido = fechacolombia #mostrar fecha partido si es el dia actual mostrar hora
        		datospartido.append(mensajepartido)
        		break
			if case('FullTime'):
				estadopartido = 'finalizado'
				datospartido.append(estadopartido)
				mensajepartido = 'FT'
				datospartido.append(mensajepartido)
				break
			if case('HalfTime'):
				estadopartido = 'encurso'
				datospartido.append(estadopartido)
				mensajepartido = 'MT'
				datospartido.append(mensajepartido)
				break
			if case('FirstHalf'):
				estadopartido = 'encurso'
				datospartido.append(estadopartido)
				tiempomsj = detalle.get('Tiempo') + "'"
				mensajepartido = tiempomsj
				datospartido.append(mensajepartido)
				break
			if case('SecondHalf'):
				estadopartido = 'encurso'
				datospartido.append(estadopartido)
				tiempomsj = detalle.get('Tiempo') + "'"
				mensajepartido = tiempomsj
				datospartido.append(mensajepartido)
				break
			if case('Live'):
				estadopartido = 'encurso'
				datospartido.append(estadopartido)
				mensajepartido = 'En juego'
				datospartido.append(mensajepartido)
				break
			estadopartido = 'encurso'
			if detalle is not None:
				tiempomsj = detalle.get('Tiempo') + "'"
				mensajepartido = tiempomsj
				datospartido.append(mensajepartido)
			else:
				mensajepartido = 'En juego'
				datospartido.append(mensajepartido)
			break
	return  datospartido

           
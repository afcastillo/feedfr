#!/usr/bin/python
# -*- coding: utf-8 -*-
'''\nScript - Generate Json calendar
Author: andari
Usage: python script.py -i <idcompetition> -t <seasoncompetition>"
Arguments:
 -i, --Id Competition:  unique id(s) of the competition(s) ej liga-aguila : 589
 -t, --Season Competition:   unique id(s) of the season(s)  ej liga-aguila : 2017
'''    
import sys
import getopt
from xml.etree import cElementTree
import datetime
import json
import timeit
import operator
import xmlestadisticas

def ArgumentError(Exception):
    pass


def usage():
    print sys.exit(__doc__)


def parse_arguments(argv):
    idcompetition = None
    season = None

    try:
        opts, args = getopt.getopt(argv[1:], "i:t:", ["idcompetition=", "seasoncompetition="])
    except getopt.GetoptError as err:
        # print help information and exit:
        print str(err) 
        usage()

    for opt, arg in opts:
        if opt == '-h':
            usage()

        elif opt in ("-i", "--idcompetition"):
            idcompetition = arg
        elif opt in ("-t", "--seasoncompetition"):
            season = arg
        else:
            raise ArgumentError("Argumento No Valido")

    if idcompetition is None or season is None:
       usage()
    return idcompetition, season

def calendar(xml):
    cteam = 0
    for elemequipos in xml.iterfind('SoccerDocument/Team'):
        cteam = cteam + 1
    cantidadequipos = cteam

    #Todos lo partidos Ordenados por el dia
    ordenado = xmlestadisticas.ordenarpordia(xml,1)
    contador = 0
    arrayPartidos = {}
    fechas = {}
    infoFechas = {}
    arrayFechas = {}
    for key,values in ordenado:
        for partido in values:
            partidos = {}
            enlacePartido = {}
            local = {}
            visitante = {}
            periodoPartido = partido['MatchInfo']['Period']
            idPartido = partido['uID']
            if  partido['MatchInfo']['RoundType']:
                fase =  partido['MatchInfo']['RoundType']
            elif  partido['MatchInfo']['RoundNumber']:
                  fase = partido['MatchInfo']['RoundNumber']  
            elif  partido['MatchInfo']['MatchType']:
                  fase = partido['MatchInfo']['MatchType']          
            numeroFecha = partido['MatchInfo']['MatchDay']
            claveFecha = numeroFecha + 'f'
            if  partido['MatchInfo']['GroupName'] is not None:
                grupoPartido =  partido['MatchInfo']['GroupName']
            else:
                grupoPartido  =  None
            fecha = partido['MatchInfo']['Date']
            fechaPartido = partido['MatchInfo']['FechaPartido']
            fechastr = datetime.datetime.strptime(fecha,'%Y-%m-%d %H:%M:%S')
            diaPartido = fechastr.strftime("%Y%m%d")
            horaPartido  = fechastr.strftime("%H:%M%S")


            #Se valida el primer partido en estado PreMatch para obtener la fecha actual (MatchDay)
            if  periodoPartido == 'PreMatch':
                if idcompetition == 625 and fase != 'Regular':
                   fechaActual = int(numeroFecha) + 32
                else:
                    fechaActual = numeroFecha
            teamreflocal = partido['TeamData']['equipo1']['TeamRef']
            equipolocal = xmlestadisticas.teamlist(teamreflocal,xml,1) 
            fichaequipolocal = xmlestadisticas.teamlist(teamreflocal,xml,2)
            mostrarMarcador = partido['TeamData']['equipo1']['Score']
            mostrarPenaltis = partido['TeamData']['equipo1']['PenaltyScore']
            teamrefvisitante = partido['TeamData']['equipo2']['TeamRef']
            equipovisitante = xmlestadisticas.teamlist(teamrefvisitante,xml,1)
            fichaequipovisitante = xmlestadisticas.teamlist(teamrefvisitante,xml,2)
            idequipolocal = teamreflocal[1:]
            idequipovisitante = teamrefvisitante[1:]
            if mostrarMarcador is not None:
               golesLocal = partido['TeamData']['equipo1']['Score']
               golesVisitante = partido['TeamData']['equipo2']['Score']
               if mostrarPenaltis is not None:
                  golesPenalLocal = partido['TeamData']['equipo1']['PenaltyScore']
                  golesPenalVisitante = partido['TeamData']['equipo2']['PenaltyScore']

            #Se define el estado del partido ( 0 : Por Jugar, 1: Finalizado, 2: En juego, 3: Aplazado)

            if  periodoPartido == 'PreMatch':
                estadoPartido = 0
            elif  periodoPartido == 'FullTime':
                  estadoPartido = 1
            elif  periodoPartido == 'Postponed':
                  estadoPartido = 3
            else:
                  estadoPartido = 2
            enlacePartido['equipoA'] =  equipolocal
            enlacePartido['equipoB'] =  equipovisitante
            enlacePartido['id'] = idPartido


            # #Información de partidos
            partidos['fecha'] = fecha
            partidos['jornada'] = numeroFecha
            partidos['fecha_partido'] = fechaPartido
            partidos['jugado'] = estadoPartido
            partidos['hora'] = horaPartido
            local['id'] = idequipolocal
            local['nombre'] = equipolocal
            if mostrarMarcador is not None:
               local['golesLocal'] = golesLocal
            else:
               local['golesLocal'] = '' 
            if mostrarPenaltis is not None:     
               local['goles_penal_local'] = golesPenalLocal
            else:
                local['goles_penal_local'] = ''   
            local['nombre_ficha'] = fichaequipolocal
            partidos['equipo_local'] = local

            visitante['id'] = idequipovisitante
            visitante['nombre'] = equipovisitante

            if mostrarMarcador is not None:
                visitante['golesVisitante'] = golesVisitante
            else:
                visitante['golesVisitante'] = ''
            if mostrarPenaltis is not None:     
               visitante['goles_penal_visitante'] = golesPenalVisitante
            else:
                visitante['goles_penal_visitante'] = ''
            visitante['nombre_ficha'] = fichaequipovisitante
            partidos['equipo_visitante'] = visitante
            partidos['estadio'] = partido['Stat']['Venue']
            partidos['ciudad'] = partido['Stat']['City']
            partidos['grupo'] = grupoPartido
            partidos['url'] = enlacePartido
            if  fase == "Todos contra todos":
                idequipolocal = None
            if  idequipolocal is not None:
                if not arrayPartidos.has_key(fase):
                    arrayPartidos[fase] = {'tipo_fase':1,'fase_activa':1,'cantidad_grupos':1,'grupos':{'nombre':'A','cantidad_equipos':cantidadequipos,'fecha_actual':numeroFecha,'fechas':{}}}
                    contador = contador + 1
                if not arrayPartidos[fase]['grupos']['fechas'].has_key(numeroFecha):
                    arrayPartidos[fase]['grupos']['fechas'][numeroFecha] = []
                arrayPartidos[fase]['grupos']['fechas'][numeroFecha].append(partidos)
                
        jsoncalendar = {'torneo':idcompetition,'temporada':season,'fase_actual':contador,'index_fase':contador-1,'fases':arrayPartidos}
                
       
        with  open('json/calendar'+'-'+idcompetition+'-'+season+'.json', 'w') as file:
              json.dump(jsoncalendar, file)      


class JsonStats():
      def __init__(self, tournament, season):
          self.tournament = idcompetition
          self.season = season
          cadena = "opta/srml-{0}-{1}-results.xml"
          cadena = cadena.format(tournament, season)
          tree = cElementTree.parse(cadena)
          calendar(tree)
 

if __name__ == "__main__":
   idcompetition, season = parse_arguments(sys.argv)
   JsonStats(idcompetition,season)